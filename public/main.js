requirejs.config({
	baseUrl: "/",
	paths: {
		"text" : "lib/text"
	},
	shim: {
		"lib/pixi" : {
			exports: "PIXI"
		},
		"lib/stats": {
			exports: "Stats"
		},
		"lib/proton": {
			exports: "Proton"
		},
		"lib/soundjs": {
			exports : "createjs.Sound"
		},
		"lib/sax": {
			exports: "sax"
		},
		"lib/xmldoc": {
			exports : "XmlDocument",
			deps: ["lib/sax"]
		},
		"lib/underscore": {
			exports: "_"
		},
		"lib/tweenjs": {
			exports: "createjs.Tween"
		},
		"lib/jquery":{
			exports: "$"
		}
	}
});

require(["game", "lib/sprintf.min", "lib/sugar", "lib/utilities"], function(game){
	var engine = new game(document.getElementById("container"));
	engine.start();
});