define(["context", "core/ui/uibase", "text!/core/ui/templates/actionmenu.hbs", "core/audio/sfx", "core/ui/aitweaker"],
function(Context, UIBase, Html, Sound, Tweaker){
	function ActionMenu(pos){
		UIBase.call(this, Html);
		this._position = pos;

		this.setPosition(pos.x, pos.y);
		
		
		var data = {
			actions: []
		};
		if (Context.game.scenario){
			for (var sKey in Context.game.scenario.actions){
				var spawn = Context.game.scenario.actions[sKey];
				data.actions.push({
					key: sKey,
					text: sprintf("%s (Cost: %s)", sKey, spawn.cost)
				});
			}
		}

		this.render(data);

		this.bindEvents({
			"#execute mouseup" : this._execute
		});
	}

	ActionMenu.extends(UIBase, {
		_execute: function(){
			var type = this.$el.find("#entitytype").val();
			var action = Context.game.scenario.actions[type];

			if (action){
				if (Context.game.removePoints(action.cost)){
					var inst = action.spawn(this._position);
					if (inst && action.adjustments)
						new Tweaker(this._position, inst, action.adjustments);
				}
				else
				{
					Sound.play("debt");
				}
			}
			this.close();
		}
	});

	return ActionMenu;
});