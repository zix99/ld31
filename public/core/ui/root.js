define(
["context",
"core/ui/uibase",
"text!/core/ui/templates/root.hbs",
"core/ui/actionmenu",
"core/ui/entitystats",
"core/ui/stats",
"core/ui/messages",
"lib/victor.min"
],
function(Context,UIBase, Html, ActionMenu, EntityStats, Stats, Messages, Vector){
	function RootWindow(){
		UIBase.call(this, Html, false);

		this.setSize(Context.view.width, Context.view.height);
		this.render();
		this.$el.mouseup(this._entityClick.bind(this));

		this._stats = new Stats();
		this.messages = new Messages();
	}

	RootWindow.extends(UIBase, {
		_update: function(){
			
		},

		_entityClick: function(e){
			if (this._active && !this._active.closed){
				this._active.close();
				this._active = null;
				return;
			}

			var clickPos = new Vector(e.offsetX, e.offsetY);
			var entities = Context.scene._entities._entities;
			for (var i=0; i<entities.length; ++i){
				var pos = new Vector( entities[i]._sprite.position.x, entities[i]._sprite.position.y );
				if (pos.distance(clickPos) < 16){
					this._active = new EntityStats(entities[i]);
					return;
				}
			}

			this._active = new ActionMenu(clickPos);
		}
	});

	return RootWindow;
});