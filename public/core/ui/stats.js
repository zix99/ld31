define(["context", "core/ui/uibase", "text!core/ui/templates/stats.hbs", "lib/underscore"],
function(Context, UIBase, Html, _){
	function Stats(){
		UIBase.call(this, Html, false);
		this.render();

		setInterval(this._update.bind(this), 500);
		this._update();

		this.$el.mouseover(this._mouseOver.bind(this));
	}

	Stats.extends(UIBase, {
		_update: function(){
			var totalHealth = this._getAverage("health", "Herbivore");
			var totalHunger = this._getAverage("hunger", "Herbivore");
			
			var data = {
				points: Context.game.getPoints(),
				stats: {
					Health: totalHealth,
					Hunger: totalHunger,
					Count: Context.scene._entities.getCountOf("Herbivore")
				},
				objectives: []
			};

			if (Context.game.scenario){
				for(var i=0; i<Context.game.scenario.goals.length; ++i){
					var goal = Context.game.scenario.goals[i];
					var objective = {
						title: goal.display,
						text: goal.goal,
						done: goal.isComplete()
					};
					data.objectives.push(objective);
				}
			}

			this.render(data);
		},

		_getAverage: function(name, ofType){
			var total=0;
			var count=0;
			for(var i=0; i<Context.scene._entities._entities.length; ++i)
			{
				var entity = Context.scene._entities._entities[i];
				if (window.isSameAs(entity, ofType)){
					if (entity[name]){
						total += entity[name];
						count++;
					}
				}
			}

			return Math.round( total / count );
		},

		_mouseOver: function(){
			this.$el.fadeOut();
			setTimeout((function(){
				this.$el.fadeIn();
			}).bind(this), 2000);
		},
	});

	return Stats;
});