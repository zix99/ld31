define(["core/ui/uibase", "text!core/ui/templates/tweaker.hbs"], function(UIBase, Html){
	function Tweaker(pos, entity, adjustments){
		UIBase.call(this, Html);

		this._entity = entity;
		this._adjustments = adjustments;

		this.setPosition(pos.x, pos.y);

		var data = {
			fields: [],
		};
		for (var aKey in adjustments){
			var adjustment = adjustments[aKey];
			data.fields.push({
				text: aKey,
				range: adjustment.range,
				val: adjustment.range[0]
			});
		}

		this.render(data);

		this.bindEvents({
			"#apply mouseup" : this._apply
		});
	}

	Tweaker.extends(UIBase, {
		_apply: function(){
			//Apply all
			for (var aKey in this._adjustments){
				var adjustment = this._adjustments[aKey];
				var val = this.$el.find("[name='" + aKey + "']").val();
				try
				{
					var nval = Math.clamp(+val, adjustment.range[0], adjustment.range[1]);
					adjustment.set(this._entity, nval || val);
				}
				catch(e){}
			}

			this.close();
		}
	});

	return Tweaker;
});