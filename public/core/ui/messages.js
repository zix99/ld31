define(["context", "lib/jquery", "core/ui/uibase", "text!core/ui/templates/messages.hbs"],
function(Context, $, UIBase, Html){
	function Messages(){
		UIBase.call(this, Html);
		this.setPosition(0, Context.view.height - 100);
		this.setSize(250, 64);
		this._messages = [];
	}

	Messages.extends(UIBase, {
		write: function(){
			var msg = sprintf.apply(null, arguments);
			this._messages.push(msg);

			while(this._messages.length > 7){
				this._messages.shift();
			}

			var data = {
				messages: this._messages
			};

			this.render(data);
			this._show();
		},

		_show: function(){
			this.$el.show();
			if (this._timer)
				clearTimeout(this._timer);
			this._timer = setTimeout(this._hide.bind(this), 5000);
		},

		_hide: function(){
			this.$el.fadeOut();
			this._timer = null;
		}
	});

	return Messages;
});