define(["lib/jquery", "lib/handlebars", "core/ui/ui"], function($, Hb, UI){
	function UiBase(templateHtml, isForm){
		this.$el = $("<div></div>");

		if (typeof isForm === "undefined" || isForm)
			this.$el.addClass("box");
		this.$el.addClass("dialog");

		//TODO: Mouse drag
		/*this.$el
			.mousedown(this._mouseDown.bind(this))
			.mouseup(this._mouseUp.bind(this));*/

		this._template = Hb.compile(templateHtml);
		UI.add(this.$el);
	}

	UiBase.prototype = {
		render: function(data){
			data = data || {};
			this.$el.html( this._template(data) );
		},
		setPosition: function(x, y){
			this.$el.css({top: y + "px", left: x + "px"});
		},
		setSize: function(width, height){
			this.$el.css({width: width + "px", height: height + "px"});
		},
		close: function(){
			this.$el.fadeOut(100, (function(){
				this.$el.remove();
			}).bind(this));
			this.closed = true;
		},

		bindEvents: function(events){
			for(var key in events){
				var parts = key.split(" ");
				this.$el
					.find(parts[0])
					.bind(parts[1], events[key].bind(this));
			}
		},

		_mouseDown: function(){
			UI.$el.mousemove(this._mouseMove.bind(this));
		},
		_mouseUp: function(){
			UI.$el.unbind("mousemove");
		},
		_mouseMove: function(e){
			console.log(e);
			UI.$el.css({
				top: e.offsetY + "px",
				left: e.offsetX + "px"
			});
		},
	};

	return UiBase;
});