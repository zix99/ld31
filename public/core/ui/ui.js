define(["lib/jquery"], function($){
	var container = $("#container");

	var root = $("<div />");
	root.addClass("ui");
	container.append(root);

	function onResize(){
		var canvas = container.find("canvas");
		var offset = canvas.offset();
		root.offset(offset);
		root.css({
			width: canvas.width() + "px",
			height: canvas.height() + "px"
		});
	}
	$(window).resize(onResize);
	setTimeout(onResize, 750); //Booo

	return {
		$el: root,

		add: function(ele){
			this.$el.append(ele);
		}
	};
});