define(["core/ui/uibase", "lib/handlebars", "text!core/ui/templates/entitystats.hbs"],
function(UIBase, Hb, Html){
	function EntityStats(entity){
		UIBase.call(this, Html);
		this._entity = entity;

		//this.setSize(200,300);
		this.setPosition(800,0);

		this._interval = setInterval(this._update.bind(this), 500);

		this._update();
	}

	EntityStats.extends(UIBase, {
		_update: function(){
			var data = {
				name: this._entity.name,
				stats: {
					Age: this._entity.age,
					Health: Math.round(this._entity.health),
					Hunger: Math.round(this._entity.hunger),
					Fatigue: Math.round(this._entity.fatigue),
					Constipation: Math.round(this._entity.constipation),
				},
				genome: this._entity.genome
			};
			this.render(data);
		},
		close: function(){
			clearInterval(this._interval);
			this.UiBase_close();
		}
	});

	return EntityStats;
});