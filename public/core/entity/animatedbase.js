define(["lib/pixi"], function(Pixi){
	function AnimatedBase(tex, fps, frameset){
		this._texture = tex;
		this._frameset = frameset;
		this._animation = null;

		this._sprite = new Pixi.Sprite(tex.frames[0]);
		this._sprite.anchor.x = 0.5;
		this._sprite.anchor.y = 0.5;

		this._frameOffset = 0;

		this._fps = 60 / fps;
		this._frameNum = 0;
	}

	AnimatedBase.prototype = {
		update: function(){
			this._frameNum++;
			if (this._frameNum >= this._fps){
				this._animate();
				this._frameNum=0;
			}
		},
		_animate: function(){
			if (this._animation in this._frameset)
			{
				var currFrameSet = this._frameset[this._animation];
				this._frameOffset = (this._frameOffset + 1) % currFrameSet.length;
				this._sprite.setTexture( this._texture.frames[ currFrameSet[this._frameOffset] ] );
			}
			else
			{
				this._sprite.setTexture(this._texture.frames[0]);
			}
		},

		setAnimation: function(name){
			if (name in this._frameset){
				if (this._animation !== name){
					this._animation = name;
					this._frameOffset=0;
					this._animate();
				}
			}
		}
	};

	return AnimatedBase;
});