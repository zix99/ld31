define(function(){
	return {
		Calm : 1,
		Attack : 2,
		Panic : 3,
		Resting : 4,
		Mating : 5,
	};
});