define(["lib/pixi"], function(pixi){
	function bunny()
	{
		var texture = pixi.Texture.fromImage("/content/sprites/bunny.png");
		this._sprite = new pixi.Sprite(texture);
		this._sprite.filters =[new pixi.InvertFilter()];
		this._sprite.anchor.x = 0.5;
		this._sprite.anchor.y = 0.5;

		this._sprite.position.x = 200;
		this._sprite.position.y = 150;
	}

	bunny.prototype = {
		update: function()
		{
			this._sprite.position.x = 32;
			this._sprite.rotation += 0.1;
		}
	};

	return bunny;
});