define([
	"context",
	"require",
	"core/entity/aibase",
 	"lib/pixi", 
 	"lib/victor.min", 
 	"core/entity/behavior", 
 	"lib/underscore",
 	"core/audio/sfx",
 	"core/entity/herd"
], 
function(Context, Require, AIBase, Pixi, Vector, Behavior, _, Sound, Herd){
	function Herbivore(spawn){
		var dots = new Pixi.ImageLoader("/content/sprites/dots.png");
		dots.load();
		AIBase.call(this, spawn, new Pixi.Texture(dots.texture, new Pixi.Rectangle(16,16,16,16)));
		
		this.genome.fatigueRate = 0.25;
		this.genome.crowdedThreshold = 12;
		this.herd = null;
	}

	Herbivore.extends([AIBase], {
		update: function(){
			this.AIBase_update();
			var carnivores = _.filter(this.visibleEntitiesByDistance(), function(e) { return window.isSameAs(e, "Carnivore"); });
			if(carnivores.length > 0)
			{
				this.setBehavior(Behavior.Panic);
				var dir = this.position().subtract(carnivores[0].position()).normalize().multiply(new Vector(100, 100));
				var dest = this.position().add(dir);
				while(dest.x < 0 || dest.y < 0 || dest.x > Context.view.width || dest.y > Context.view.height)
				{
					dir.rotateByDeg(5);
					dest = this.position().add(dir);
				}
				this.setDestination(dest);
			}
			else
			{
				if(this._behavior != Behavior.Mating)
				{
					this.setBehavior(Behavior.Calm);
				}
			}
		},

		setBehavior: function(behavior){
			this.AIBase_setBehavior(behavior);
			switch(this._behavior)
			{
				case Behavior.Mating:
					this.setSpeedMultiplier(0.25);
					break;
				case Behavior.Calm:
					this.setSpeedMultiplier(0.25);
					break;
				case Behavior.Panic:
					this.setSpeedMultiplier(1);
					break;
			}
		},

		fatigued: function(){
		},

		rested: function(){
		},

		kill: function(){
			this.herd.leave(this);
			this.AIBase_kill();
		},

		chooseHerd: function(){
			if(Context.herds.length === 0)
			{
				var herd = new Herd();
				this.herd = herd;
			}
			else
			{
				var herds = _.sortBy(Context.herds, _.bind(function(e){ 
					return e.position().distance(this.position()); 
				}, this));
				this.herd = herds[0];
			}
			this.herd.join(this);
		},

		reproduce: function(){
			if(this.herd.size() > this.genome.crowdedThreshold)
			{
				this.herd.leave(this);
				this.mate.herd.leave(this.mate);

				var herd = new Herd();
				this.herd = herd;
				this.mate.herd = herd;
				herd.join(this);
				herd.join(this.mate);
			}
			this.AIBase_reproduce();
		},

		childSpawned: function(child){
			child.herd = this.herd;
			this.herd.join(child);
		},

		getRange: function(){
			if(this.herd === null)
			{
				this.chooseHerd();
			}

			return this.herd.range();
		},

		atDestination: function(){
			if(this._behavior == Behavior.Mating)
			{
				this.seekMate();
			}
			else if(this._behavior == Behavior.Calm)
			{
				var pos = this.position();
				var near = Context.environment.getEdibleNear(pos, 16);
				if (near){
					near.kill();
					this.hunger = Math.max(0, this.hunger - 40);
					this.constipation += 30;
					Sound.play("nom-" + Math.randomInt(1, 5));
				}

				var edibleNear = Context.environment.getEdibleNear(pos, this.genome.sight);
				if (edibleNear && this.hunger > 25)
				{
					this.setDestination(edibleNear.getPosition());
				}
				else
				{
					var range = this.getRange();
					var rand = new Vector().randomize(range[0], range[1]);
					this.setDestination(rand);
				}
			}
		}

	});

	return Herbivore;
});