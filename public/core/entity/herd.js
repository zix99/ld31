define(["context", "lib/victor.min", "lib/underscore", "lib/pixi"], function(Context, Vector, _, Pixi){
	function Herd(){
		this._members = [];
		//this._graphics = new Pixi.Graphics();
		//Context.scene.getStage().addChild(this._graphics);
		Context.herds.push(this);
	}

	var gridSize = 128;
	var biasSize = 64;
	var herdSize = 48;

	function toGrid(pos)
	{
		return Math.floor(Math.clamp(pos.x, 0, Context.view.width - 1) / gridSize) + Math.floor(Math.clamp(pos.y, 0, Context.view.height - 1)  / gridSize) * (Context.view.width / gridSize);
	}

	Herd.prototype = {
		join: function(entity){
			this._members.push(entity);
		},

		leave: function(entity){
			this._members = _.without(this._members, entity);
			if(this.size() === 0)
			{
				Context.herds = _.without(Context.herds, this.herd);
			}
		},

		size: function(){
			return this._members.length;
		},

		position: function(){

			var raw = this.rawPosition();
			var pos = raw.add(this.bias());
			/*
			this._graphics.beginFill(0xFF0000);
			this._graphics.drawCircle(pos.x, pos.y, 8);
			this._graphics.endFill();
			*/
			return pos;
		},

		rawPosition: function(){
			var sum = _.reduce(this._members, function(a, b){ return a.add(b.position()); }, new Vector());
			return sum.divide(new Vector(this._members.length, this._members.length));
		},

		bias: function(){
			var grid =  _.sortBy(this.herdMap(), _.bind(function(g){
				var val = 0;
				if(g.plants === 0)
				{
					return val;
				}
				val += g.plants - (this.rawPosition().distance(g.position) / (gridSize  * 0.75));
				val = Math.clamp(val, 1, 100);
				val -= g.herds;

				if(g.position.clone().subtract(this.rawPosition()).length() < biasSize)
				{
					val += this.size();
				}
				if(g.beacon)
				{
					val += 500;
				}

				return val;
			}, this));
			/*
			this._graphics.clear();
			this._graphics.beginFill(0x0000FF);
			this._graphics.drawCircle(_.last(grid).position.x, _.last(grid).position.y, 8);
			this._graphics.endFill();
			this._graphics.beginFill(0x00FF00);
			this._graphics.drawCircle(this.rawPosition().x, this.rawPosition().y, 8);
			this._graphics.endFill();
			*/
			var bias = _.last(grid).position.clone().subtract(this.rawPosition());
			if(bias.length() < biasSize)
			{
				return new Vector();
			}

			bias.normalize().multiply(new Vector(biasSize, biasSize));
			return bias;
		},

		herdMap: _.throttle(function(){
			var grid = [];
			for(var y = gridSize/2; y < Context.view.height; y += gridSize)
			{
				for(var x = gridSize/2; x < Context.view.width; x += gridSize)
				{
					grid.push({position: new Vector(x, y), plants: 0, herds: 0, beacon: false});
				}
			}

			_.each(Context.environment._entities, function(e){
				var pos = e.getPosition();
				if(e.edible)
				{
					grid[toGrid(pos)].plants++;
				}
				if(window.isSameAs(e, "Beacon"))
				{
					grid[toGrid(pos)].beacon = true;
				}
			});

			_.each(Context.herds, function(herd){
				var pos = herd.rawPosition();
				grid[toGrid(pos)].herds += herd.size();
			});

			return grid;

		}, 2000),

		range: function(){
			var pos = this.position();
			return [new Vector(-48, -48).add(pos), new Vector(herdSize, herdSize).add(pos)];
		}
	};

	Context.herds = [];
	return Herd;
});