define(["text!content/names.txt", "lib/underscore"], function(NameData, _){
	var names = _.filter(NameData.split("\n"), function(s){
		return s !== "";
	});


	return {
		getName: function(){
			return names[Math.randomInt(0, names.length)];
		}
	};
});