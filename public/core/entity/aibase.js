define(["context", "lib/pixi", "lib/victor.min", "lib/underscore", "core/entity/behavior", "core/audio/sfx", "core/entity/namer"],
function(Context, Pixi, Vector, _, Behavior, Sound, Namer){
	function AIBase(spawn, texture){
		this.name = Namer.getName();
		Context.messages.write("<strong>%s</strong> was born", this.name);

		this._sprite = new Pixi.Sprite(texture);
		this._sprite.anchor.x = 0.5;
		this._sprite.anchor.y = 0.5;
		this._sprite.scale = new Pixi.Point(0.5, 0.5);

		this._destination = Vector.fromObject(spawn);
		this._speedMultiplier = 1;

		this._sprite.position.x = spawn.x;
		this._sprite.position.y = spawn.y;

		this.genome = {
			sight: 100,
			topSpeed: 2,
			fatigueRate: 0.5,
			hungerRate: 0.02,
			heatRate: 0.05,
			lifespan: 10,
			litterSize: 4,
			digestion: 6,
		};

		this.fatigue = 0;
		this.health = 100;
		this.hunger = 0;
		this.heat = 100 * Math.random();
		this.mate = null;
		this.age = 0;
		this.constipation=0;

		this._poopTick = 0;

		this.yearEnd = _.throttle(this.yearEnd, yearLength, {leading: false});
		this.visibleEntitiesByDistance = _.throttle(this.visibleEntitiesByDistance, 250);

		this.setBehavior(Behavior.Calm);
	}

	var yearLength = 1000 * 45; // 45 seconds

	AIBase.prototype = {
		update: function(){
			// Move to target
			if(this._destination !== null)
			{
				var d = this._destination.clone().subtract(this.position());
				if(d.length() > this.currentSpeed().length())
				{
					d.normalize().multiply(this.currentSpeed());
					var newPos = d.clone().add(new Vector(this._sprite.position.x, this._sprite.position.y));
					if (this._canMoveTo(newPos))
					{
						this._sprite.position.x = newPos.x;
						this._sprite.position.y = newPos.y;
					}
					else
					{
						this._destination = null;
						this.atDestination();
					}
				}
				else
				{
					this._destination = null;
					this.atDestination();
				}
			}

			// Heat
			this.heat += !this.isYoung() ? this.genome.heatRate : 0; 
			if(this.heat >= 100 && this.mate === null && this._behavior != Behavior.Panic && this.hunger < 50)
			{					
				this.chooseMate();
			}
			this.heat = Math.clamp(this.heat, 0, 100);

			// Hunger
			this.hunger += this.genome.hungerRate;
			if(this.hunger >= 100)
			{
				this.damage(0.25);
			}
			this.hunger = Math.clamp(this.hunger, 0, 100);

			// Fatigue
			if(this._speedMultiplier > 0.5)
			{
				this.fatigue += this._speedMultiplier * this.genome.fatigueRate;
			}
			else
			{
				this.fatigue -= (1 - this._speedMultiplier) * this.genome.fatigueRate;
			}
			this.fatigue = Math.clamp(this.fatigue, 0, 100);

			if(this.fatigue == 100)
			{
				this.fatigued();
			}
			if(this.fatigue === 0)
			{
				this.rested();
			}

			//Poopin
			if (this.constipation >= 50){
				this._poopTick++;
				if (this._poopTick > this.genome.digestion * 60){
					this._poopTick = 0;
					this.constipation = Math.max(0, this.constipation - 80);
					Context.environment.dropPoop(this.position());
				}
			}
			if (this.constipation > 100){
				this.damage(0.1);
			}

			this.yearEnd();
		},

		_canMoveTo: function(){
			return true;
		},

		yearEnd: function(){
			this.age++;
			if(this.age > this.genome.lifespan)
			{
				this.kill();
			}

			if(this.isYoung())
			{
				this._sprite.scale = new Pixi.Point(0.75, 0.75);
			}
			else
			{
				this._sprite.scale = new Pixi.Point(1, 1);
			}

			if(this.isOld())
			{
				this._sprite.tint = 0xCCCCCC;
			}
		},

		chooseMate: function(){
			var mates = _.filter(Context.scene._entities._entities, _.bind(function(e){ 
				return e != this && e.heat >= 100 && e.mate === null && this.sameClass(e); 
			}, this));

			if(mates.length > 0)
			{
				mates[0].mate = this;
				this.mate = mates[0];
				this.setBehavior(Behavior.Mating);
				this.mate.setBehavior(Behavior.Mating);
				this.seekMate();
			}
		},

		seekMate: function(){
			if(this.mate !== null && this.mate._behavior == Behavior.Mating && this.mate.mate == this)
			{
				if(this.mate.position().distance(this.position()) < 16 )
				{
					this.reproduce();
				}
				else
				{
					var meetup = this.position().mix(this.mate.position());
					this.setDestination(meetup);
					this.mate.setDestination(meetup);
				}
			}
			else
			{
				this.setBehavior(Behavior.Calm);
				this.mate = null;
				this.atDestination();
			}
		},

		reproduce: function(){
			var avgSize = (this.genome.litterSize + this.mate.genome.litterSize)/2.0;
			var count = Math.round(avgSize + ((Math.random() - 0.5) * 2));
			var doGenetics = _.bind(function(value, gene){
					child.genome[gene] =  Math.random() > 0.5 ? this.genome[gene] : this.mate.genome[gene];
					child.genome[gene] += child.genome[gene] * (Math.random() > 0.5 ? 0.1 : -0.1);
				}, this);

			for(var i = 0; i < count; i++)
			{
				var child = new this.constructor(this.position());

				_.each(child.genome, doGenetics);

				Context.scene._entities.add(child);

				this.childSpawned(child);
			}

			this.clearMating();
			Sound.play("doink-" + Math.randomInt(1, 6));
		},

		childSpawned: function(){},

		clearMating: function()
		{
			this.mate.setBehavior(Behavior.Calm);
			this.setBehavior(Behavior.Calm);
			this.mate.heat = 0;
			this.heat = 0;
			this.atDestination();
			this.mate.atDestination();
			this.mate.mate = null;
			this.mate = null;
		},

		setBehavior: function(behavior){
			this._behavior = behavior;
		},

		fatigued: function(){
		},

		rested: function(){
		},

		damage: function(amount){
			this.health -= amount;
			if(this.health <= 0)
			{
				this.kill();
			}
		},

		kill: function(){
			Context.messages.write("<strong>%s</strong> died!", this.name);
			Sound.play("meh-" + Math.randomInt(1,6));
			Context.scene._entities.remove(this);
		},

		position: function(){
			return Vector.fromObject(this._sprite.position);
		},

		setTexture: function(texture){
			this._sprite = new Pixi.Sprite(texture);
			this._sprite.anchor.x = 0.5;
			this._sprite.anchor.y = 0.5;
		},

		setDestination: function(destination){
			this._destination = Vector.fromObject(destination);
		},

		clearDestination: function(){
			this._destination = null;
		},

		currentSpeed: function(){
			var mult = this.fatigue == 100 ? 0.1 : this._speedMultiplier; // supa slow if fatigued
			mult *= this.isYoung() || this.isOld() ? 0.75 : 1;
			return new Vector(this.genome.topSpeed, this.genome.topSpeed).clone().multiply(new Vector(mult, mult));
		},

		setSpeedMultiplier: function(multiplier){
			this._speedMultiplier = multiplier;
		},

		isYoung: function(){
			return this.age < this.genome.lifespan * 0.25;
		},

		isOld: function(){
			return this.age > this.genome.lifespan * 0.75;
		},

		visibleEntities: function(){
			return _.filter(Context.scene._entities._entities, _.bind(function(e){ 
				return e != this && e.position().distance(this.position()) < this.genome.sight; 
			}, this));
		},

		visibleEntitiesByDistance: function(){
			return _.sortBy(this.visibleEntities(), _.bind(function(e){ 
				return e.position().distance(this.position()); 
			}, this));
		},

		sameClass: function(other){
			return other instanceof this.constructor;
		}
	};

	return AIBase;
});