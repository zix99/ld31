define([
	"require",
	"context", 
	"core/entity/aibase", 
	"core/entity/herbivore", 
	"lib/pixi", 
	"lib/victor.min", 
	"lib/underscore", 
	"core/entity/behavior"
], 
function(Require, Context, AIBase, Herbivore, Pixi, Vector, _, Behavior){
	function Carnivore(spawn){
		var dots = new Pixi.ImageLoader("/content/sprites/dots.png");
		dots.load();
		AIBase.call(this, spawn, new Pixi.Texture(dots.texture, new Pixi.Rectangle(0,0,16,16)));
	}

	Carnivore.extends([AIBase], {
		update: function(){
			this.AIBase_update();
			if(this._behavior != Behavior.Rest && this._behavior != Behavior.Mating)
			{
				var targets = _.filter(this.visibleEntitiesByDistance(), function(e) { return window.isSameAs(e, "Herbivore"); });
				if(targets.length > 0 && this.hunger > 50)
				{
					if(this._behavior == Behavior.Calm)
					{
						this.setBehavior(Behavior.Attack);
					}

					if(targets[0].position().distance(this.position()) < 8)
					{
						Context.messages.write("<strong>%s</strong> ate <strong class=\"red\">%s</strong>", this.name, targets[0].name);
						this.hunger -= targets[0].health;
						this.constipation += targets[0].health;
						targets[0].kill();
					}
					this.setDestination(targets[0].position());
				}
				else
				{
					this.setBehavior(Behavior.Calm);
				}
			}
		},

		setBehavior: function(behavior){
			this.AIBase_setBehavior(behavior);
			switch(this._behavior)
			{
				case Behavior.Mating:
					this.setSpeedMultiplier(0.5);
					break;
				case Behavior.Calm:
					this.setSpeedMultiplier(0.5);
					break;
				case Behavior.Attack:
					this.setSpeedMultiplier(1);
					break;
				case Behavior.Rest:
					this.setSpeedMultiplier(0);
					this.clearDestination();
					break;
			}
		},

		fatigued: function(){
			this.setBehavior(Behavior.Rest);
		},

		rested: function(){
			if(this._behavior == Behavior.Rest)
			{
				this.setBehavior(Behavior.Calm);
			}
		},

		atDestination: function(){
			if(this._behavior == Behavior.Mating)
			{
				this.seekMate();
			}
			else
			{
				var rand = new Vector().randomize(new Vector(0,0), new Vector(Context.view.width, Context.view.height));
				this.setDestination(rand);
			}
		},

		_canMoveTo: function(pos){
			var totem = Context.environment.getTotemNear(pos);
			return totem ? false : true;
		},

		kill: function(){
			Context.game.addPoints(1);
			this.AIBase_kill();
		}
	});

	return Carnivore;
});