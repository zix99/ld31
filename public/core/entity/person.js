define(["core/entity/animatedbase", "core/entity/physicsbase", "lib/pixi", "core/lib/keyboard"], function(AnimatedBase, PhysicsBase, pixi, kb){
	function Person(){
		var texture = this._texture = new pixi.ImageLoader("/content/sprites/characters/officeman2.png");
		texture.loadFramedSpriteSheet(32, 48);

		var frameset = {
			stand: [0],
			down: [0,1,2,3],
			left: [4,5,6,7],
			right: [8,9,10,11],
			up: [12,13,14,15]
		};

		AnimatedBase.call(this, texture, 4, frameset);
		PhysicsBase.call(this);

		this._sprite.position.x = 200;
		this._sprite.position.y = 50;
	}

	var speed = 2;

	Person.extends([AnimatedBase, PhysicsBase], {
		update: function(){
			var isMoving = false;
			if (kb.isDown("a")){
				this._sprite.position.x -= speed;
				this.setAnimation("left");
				isMoving = true;
			}
			if (kb.isDown("d")){
				this._sprite.position.x += speed;
				this.setAnimation("right");
				isMoving = true;
			}
			if (kb.isDown("w")){
				this._sprite.position.y -= speed;
				this.setAnimation("up");
				isMoving = true;
			}
			if (kb.isDown("s")){
				this._sprite.position.y += speed;
				this.setAnimation("down");
				isMoving = true;
			}

			if (!isMoving){
				this.setAnimation("stand");
			}

			this.AnimatedBase_update();
			this.PhysicsBase_update();
		}
	});

	return Person;
});