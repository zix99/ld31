define(function(){
	function PhysicsBase(){
		this.world = null;
	}

	PhysicsBase.prototype = {
		update: function(){
			if (this.world){
				if (this.world.isCollided(this._sprite.x, this._sprite.y)){
					this._revertPosition();
				}

				var box = this.world.getBox(this._sprite.x, this._sprite.y);
				if (box){
					switch(box.type){
						case "portal":
						var target = box.props.target.split(",");
						this._sprite.x = +target[0];
						this._sprite.y = +target[1];
						break;
					}
				}

				var cell = this.world.getCell(this._sprite.x, this._sprite.y);
				if (cell){
					if (cell.props.collision){
						this._revertPosition();
					}
				}
			}

			if (this._sprite.x < 0)
				this._sprite.x = 0;
			if (this._sprite.y < 0)
				this._sprite.y = 0;

			this._lastX = this._sprite.x;
			this._lastY = this._sprite.y;
		},

		_revertPosition: function(){
			this._sprite.x = this._lastX;
			this._sprite.y = this._lastY;
		}
	};

	return PhysicsBase;
});