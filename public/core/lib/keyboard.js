define(function(){
	var keyState = {};
	var keyBinds = {};

	function invokeBind(code, isDown)
	{
		if (code in keyBinds)
		{
			for(var i=0; i<keyBinds[code].length; ++i)
			{
				try{
					keyBinds[code][i](code, isDown);
				}catch(e){
					console.log(e);
				}
			}
		}
	}

	function onKeyDown(evt){
		var code = evt.keyCode;
		keyState[code] = true;
		invokeBind(code, true);
	}

	function onKeyUp(evt){
		var code = evt.keyCode;
		keyState[code] = false;
		invokeBind(code, false);
	}

	window.addEventListener("keydown", onKeyDown);
	window.addEventListener("keyup", onKeyUp);

	function getCharCode(symbol){
		if (typeof symbol === "string")
		{
			return symbol.toUpperCase().charCodeAt(0);
		}
		return symbol;
	}

	return {
		isDown: function(keycode){
			var code = getCharCode(keycode);
			return code in keyState && keyState[code];
		},
		bind: function(keycode, func){
			var code = getCharCode(keycode);
			if (!(code in keyBinds))
				keyBinds[code] = [];
			keyBinds[code].push(func);
		},
		isBound: function(keycode){
			var code = getCharCode(keycode);
			return code in keyBinds;
		},
		clearBinds: function(){
			keyBinds = {};
		}
	};
});