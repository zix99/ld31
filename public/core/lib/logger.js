define(function(){
	return {
		_log: function(level, msg, fmtArgs)
		{
			var fmtString = sprintf(msg, fmtArgs);
			var s = sprintf("[%s] %s", level, fmtString);
			console.log(s);
		},
		logInfo: function(msg)
		{
			this._log("INFO", msg, Array.prototype.slice.call(arguments, 1));
		},
		logWarning: function(msg)
		{
			this._log("WARN", msg, Array.prototype.slice.call(arguments, 1));
		},
		logError: function(msg)
		{
			this._log("ERROR", msg, Array.prototype.slice.call(arguments, 1));
		}
	};
});