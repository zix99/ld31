define(function(){
	function defaultFor(val, def)
	{
		return typeof val !== "undefined" ? val : def;
	}

	function Task(func, that)
	{
		this._func = func;
		this._paused = false;
		this._task = null;
		this._that = defaultFor(that, this);
	}

	Task.prototype = {
		start : function(freq)
		{
			if (this._task !== null)
				throw "Task already started";
			this._task = window.setInterval( this._execute.bind(this._that), freq);
		},
		stop : function()
		{
			clearInterval(this._task);
			this._task = null;
		},
		pause : function()
		{
			this._paused = true;
		},
		unPause: function()
		{
			this._paused = false;
		},
		_execute: function()
		{
			if (!this._paused)
			{
				var ret = this._func();
				if (ret)
					this.stop();
			}
		}
	};

	return Task;
});