define(["lib/underscore", "lib/pixi"], function(_, Pixi){
	//Singleton

	function EntityManager(){
		this._entities = [];
		this._stage = new Pixi.DisplayObjectContainer();
	}

	EntityManager.prototype = {
		add: function(entity)
		{
			this._stage.addChild(entity._sprite);
			this._entities.push(entity);
		},

		remove: function(entity)
		{
			this._stage.removeChild(entity._sprite);
			this._entities = _.without(this._entities, entity);
		},

		getCount: function(){
			return this._entities.length;
		},

		getCountOf: function(type){
			var total=0;
			for(var i=0; i<this._entities.length; ++i)
			{
				if (window.isSameAs(this._entities[i], type)){
					total++;
				}
			}
			return total;
		},

		update: function()
		{
			for (var i=0; i<this._entities.length; i++)
			{
				this._entities[i].update();
			}
		},

		getStage: function(){
			return this._stage;
		},

		getEntities: function(){
			return this._entities;
		}
	};

	return EntityManager;
});