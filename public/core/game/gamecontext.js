define(["core/game/scenario", "core/audio/sfx", "lib/underscore"], function(Scenario, Sound, _){
	function GameContext(){
		this._points = 0;
		this._tick = 0;
		this.scenario = null;

		setInterval(this._checkCompletion.bind(this), 2000);
		setInterval(this._tickupdate.bind(this), 1000);
	}

	GameContext.prototype = {
		getPoints: function(){
			return this._points;
		},
		addPoints: function(pts){
			this._points += pts;
			Sound.play("kaching");
		},
		removePoints: function(cost){
			if (this._points >= cost)
			{
				this._points -= cost;
				return true;
			}
			return false;
		},

		startScenario: function(scenario){
			this.scenario = scenario;
			Sound.play("darwinism-" + Math.randomInt(1, 7));
			if (this.scenario){
				this.addPoints(scenario.startPoints);
				if (this.scenario.start)
					this.scenario.start();
			}
		},
		start: function(num){
			num = Math.clamp(num || 0, 0, Scenario.length);
			this.startScenario(Scenario[num]);
		},

		progress: function(){
			//Reward!
			this.addPoints(this.scenario.rewardPoints);
			if (this.scenario.finish)
				this.scenario.finish();

			var nextScenario = this.scenario.nextScenario;
			this.startScenario(nextScenario);
		},

		fail: function(){
			//restart the current scenario
			this.startScenario(this.scenario);
			//Also, maybe play a sad sound
		},

		_tickupdate: function(){
			this._tick++;
			if (this.scenario && this.scenario.natural){
				for (var i=0; i<this.scenario.natural.length; ++i){
					var evt = this.scenario.natural[i];
					if (this._tick % evt.ticks === 0){
						evt.do();
					}
				}
			}
		},

		_checkCompletion: function(){
			if (this.scenario){
				var completed = _.every(this.scenario.goals, function(goal){
					return goal.isComplete();
				});
				if (completed){
					this.progress();
				}

				//Fail?
				if (this.scenario && this.scenario.isFailed && this.scenario.isFailed()){
					this.fail();
				}
			}
		}
	};

	return GameContext;
});