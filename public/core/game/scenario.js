define(
[
	"context",
	"core/entity/herbivore",
	"core/entity/carnivore",
	"lib/underscore",
	"lib/victor.min",
	"core/environment/plants/tree",
	"core/environment/elements/totem",
	"core/environment/elements/megatotem",
	"core/environment/events/fireball",
	"core/environment/elements/beacon"
],
function(Context, Herbivore, Carnivore, _, Vector, Tree, Totem, MegaTotem, Fireball, Beacon){

	function genericIsFailed(){
		return Context.scene._entities.getCountOf("Herbivore") === 0;
	}

	function randomPoint(){
		return new Vector(Math.randomRange(0, Context.view.width), Math.randomRange(0, Context.view.height));
	}

	var defaultActions = {
			"Totem": {
				cost:2,
				spawn: function(pos){
					var inst = new Totem(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
			"Carnivore": {
				cost: 5,
				adjustments: {
					"Top Speed" : { range : [0.1,3], set: function(e, val) { e.genome.topSpeed = +val; }  }
				},
				spawn: function(pos)
				{
					var inst = new Carnivore(pos);
					Context.scene._entities.add(inst);
					return inst;
				}
			},
			"Green Time": {
				cost: 50,
				spawn: function(){
					Context.environment.setRate(50);
					setTimeout(function(){
						Context.environment.setRate(1);
					}, 5000);
				}
			},
			"Plant Tree": {
				cost: 5,
				spawn: function(pos){
					var tree = new Tree(pos);
					Context.environment.add(tree);
				}
			}
		};

	var finalScenario = {
		goals: {
			display: "SO MUCH FUN",
			goal: "You WILL have all the fun",
			isComplete: function(){
				return false;
			}
		},

		actions: defaultActions,

		startPoints: 10000,

		start: function(){
			Context.messages.write("Entering win mode");
		}

	};

	var Scenario3 = {
		goals: [
			{
				display: "Vengence", 
				goal: "Drive the Herbivores to extinction",
				isComplete: function(){
					return Context.scene._entities.getCountOf("Herbivore") === 0;
				}
			},
		],

		actions: {
			"Hungry Carnivore": {
				cost: 5,
				spawn: function(pos)
				{
					var inst = new Carnivore(pos);
					inst.genome.topSpeed = 4;
					inst.genome.fatigueRate = 0;
					inst.genome.hungerRate *= 10;
					inst.hunger = 50;
					Context.scene._entities.add(inst);
					return inst;
				}
			},
			"Fireball" : {
				cost: 10,
				spawn: function(pos){
					new Fireball(pos);
				}
			},
			"Beacon": {
				cost:2,
				spawn: function(pos){
					var inst = new Beacon(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
		},

		natural: [],

		startPoints: 30,
		rewardPoints: 20,
		nextScenario: finalScenario
	};


	var Scenario2 = {
		goals: [
			{
				display: "Dawn of an Era", 
				goal: "Get the population to at least 50",
				isComplete: function(){
					return Context.scene._entities.getCountOf("Herbivore") >= 50;
				}
			},
		],

		actions: {
			"Totem": {
				cost:2,
				spawn: function(pos){
					var inst = new Totem(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
			"Fireball" : {
				cost: 10,
				spawn: function(pos){
					new Fireball(pos);
				}
			},
			"Mega Totem": {
				cost:7,
				spawn: function(pos){
					var inst = new MegaTotem(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
			"Beacon": {
				cost:2,
				spawn: function(pos){
					var inst = new Beacon(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
		},

		natural: [
			{
				ticks: 30,
				do: function(){
					var ent = new Carnivore(randomPoint());
					Context.scene._entities.add(ent);
				}
			}
		],

		startPoints: 30,
		rewardPoints: 20,
		nextScenario: Scenario3
	};

	var Scenario1 = {
		goals: [
			{
				display: "It's Booming",
				goal: "Get a population of at least 20. Protect them with <strong>Totems</strong>!",
				isComplete: function(){
					return Context.scene._entities.getCountOf("Herbivore") >= 20;
				}
			},
		],

		actions: {
			"Totem": {
				cost:2,
				spawn: function(pos){
					var inst = new Totem(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
			"Fireball" : {
				cost: 10,
				spawn: function(pos){
					new Fireball(pos);
				}
			},
			"Beacon": {
				cost:2,
				spawn: function(pos){
					var inst = new Beacon(pos);
					Context.environment.add(inst);
					return inst;
				}
			},
			"Plant Tree": {
				cost: 1,
				spawn: function(pos){
					var tree = new Tree(pos);
					Context.environment.add(tree);
				}
			}
		},

		natural: [
			{
				ticks: 50,
				do: function(){
					var ent = new Carnivore(randomPoint());
					Context.scene._entities.add(ent);
				}
			}
		],

		start: function(){
			//Spawn herds
			var entities = Context.scene._entities;
			var pt = new Vector(Math.randomInt(0, Context.view.width), Math.randomInt(0, Context.view.height));
			var h1 = new Herbivore(pt);
			h1.age = 3;
			var h2 = new Herbivore(pt);
			h2.age = 3;
			entities.add(h1);
			entities.add(h2);
			entities.add(new Herbivore(pt));
			entities.add(new Herbivore(pt));
		},

		finish: function(){
			//Clean up anything
		},

		isFailed: genericIsFailed,

		startPoints: 20,
		rewardPoints: 20,
		nextScenario: Scenario2
	};

	return [
		Scenario1,
		Scenario2,
		Scenario3
	];
});