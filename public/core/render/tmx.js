define(["lib/xmldoc", "lib/pixi", "lib/underscore"], function(XmlDocument, Pixi, _){
	function copyPropertiesToObject(xml, obj){
		var properties = xml.childrenNamed("property");
		for (var i=0; i<properties.length; ++i){
			var name = properties[i].attr.name;
			var val = properties[i].attr.value;
			obj[name] = val;
		}
	}

	function Tmx(xml, basePath){
		var i,j,x,y;

		if (!xml)
			throw Error("You need to give us some XML");

		var doc = new XmlDocument(xml);

		this._basePath = basePath || "";

		this.width = +doc.attr.width;
		this.height = +doc.attr.height;
		this.tilewidth = +doc.attr.tilewidth;
		this.tileheight = +doc.attr.tileheight;
		this.screenWidth = this.width * this.tilewidth;
		this.screenHeight = this.height * this.tileheight;

		//Load tileset
		this.tilesets = [];
		this.terrains = {};

		var tileData = doc.childrenNamed("tileset");
		for (i=0; i<tileData.length; ++i){
			var ts = tileData[i];
			var tileset = {
				width: +ts.attr.tilewidth,
				height: +ts.attr.tileheight,
				name: ts.attr.name,
				image: ts.childNamed("image").attr.source,
				startId: +ts.attr.firstgid,
				props: {},
				tiles: {}
			};

			//Load properties
			var tilesetProps = ts.childNamed("properties");
			if (tilesetProps)
				copyPropertiesToObject(tilesetProps, tileset.props);

			//Load per-tile properties
			var tilesetTiles = ts.childrenNamed("tile");
			for (j=0; j<tilesetTiles.length; ++j){
				var tile = {
					id: +tilesetTiles[j].attr.id + tileset.startId,
					props: {}
				};

				var tilesetTileProps = tilesetTiles[j].childNamed("properties");
				if (tilesetTileProps)
					copyPropertiesToObject(tilesetTileProps, tile.props);

				tileset.tiles[tile.id] = tile;
			}

			//Load terrain types
			var terrainTypes = ts.childNamed("terraintypes");
			if (terrainTypes){
				var terrainItems = terrainTypes.childrenNamed("terrain");
				for(j=0; j<terrainItems.length; ++j){
					var terrainName = terrainItems[j].attr.name;
					var tileId = +terrainItems[j].attr.tile;
					this.terrains[terrainName] = tileId + tileset.startId;
				}
			}

			this.tilesets.push(tileset);
		}

		//Load layers
		this.layers = [];
		this.layerNameMap = {};
		var dataLayers = doc.childrenNamed("layer");
		for (i=0; i<dataLayers.length; ++i){
			var l = dataLayers[i];
			this.layers[i] = {
				name: l.attr.name,
				width: +l.attr.width,
				height: +l.attr.height,
				data: [],
				props: {}
			};
			this.layerNameMap[l.attr.name] = i;

			//Get properties
			var props = l.childNamed("properties");
			if (props){
				copyPropertiesToObject(props, this.layers[i].props);
			}

			//Get data
			var ld = l.childNamed("data");
			if (ld.attr.encoding != "csv")
				throw Error("Expected csv encoding");

			//Init data cells
			for (x=0; x<l.attr.width; ++x)
				this.layers[i].data[x] = [];

			//Parse data
			var rows = ld.val.trim().split("\n");
			for (y=0; y<l.attr.height; ++y){
				var cols = rows[y].trim().split(",");
				for (x=0;x<l.attr.width; ++x){
					this.layers[i].data[x][y] = +cols[x];
				}
			}
		}

		//Get collision groups
		this._collisionLayers = _.filter(this.layers, function(e){ return e.props.collision ? true : false; });

		//Load all box object groups
		this.boxes = [];
		var dataObjects = doc.childrenNamed("objectgroup");
		for (i=0; i<dataObjects.length; ++i){
			var children = dataObjects[i].childrenNamed("object");
			for (j=0; j<children.length; ++j){
				var o = children[j];
				var box = {
					name: o.attr.name,
					type: o.attr.type,
					x: +o.attr.x,
					y: +o.attr.y,
					width: +o.attr.width,
					height: +o.attr.height,
					props: {}
				};

				var objProps = o.childNamed("properties");
				if (objProps)
					copyPropertiesToObject(objProps, box.props);

				this.boxes.push(box);
			}
		}


		this.tint = 0xffffff;
		this.filters = [];
	}

	Tmx.prototype = {
		getBox: function(x, y){
			for(var i=0; i<this.boxes.length; ++i){
				var box = this.boxes[i];
				if (!(x < box.x || x >= box.x + box.width || y < box.y || y >= box.y + box.height))
					return box;
			}
			return null;
		},

		isCollided: function(x,y){
			var cellX = Math.floor(x / this.tilewidth);
			var cellY = Math.floor(y / this.tileheight);

			for (var i=0; i<this._collisionLayers.length; ++i){
				var layer = this._collisionLayers[i];
				if (cellX >= 0 && cellX < layer.width && cellY >= 0 && cellY < layer.height)
				{
					var tileId = layer.data[cellX][cellY];
					if (tileId > 0)
						return true;
				}
			}
			return false;
		},

		getCell: function(x,y){
			var cellX = Math.floor(x / this.tilewidth);
			var cellY = Math.floor(y / this.tileheight);

			for (var i=this.layers.length-1; i>=0; --i){
				var layer = this.layers[i];
				if (cellX >= 0 && cellY >= 0 && cellX < layer.width && cellY < layer.height){
					var tileId = layer.data[cellX][cellY];
					if (tileId > 0){
						return this._getTileInfo(tileId);
					}
				}
			}
			return null;
		},

		getLayerCell: function(layer, x, y){
			var cellX = Math.floor(x / this.tilewidth);
			var cellY = Math.floor(y / this.tileheight);
			var tileId = this.layers[layer].data[cellX][cellY];
			if (tileId > 0){
				return this._getTileInfo(tileId);
			}
			return null;
		},

		_getTileInfo: function(tileId){
			var tileset = this._getCellTileset(tileId);
			return tileset.tiles[tileId] || {id: tileId, props: {}};
		},

		setCellByName: function(x,y,layer,name){
			var cellX = Math.floor(x / this.tilewidth);
			var cellY = Math.floor(y / this.tileheight);

			var cellId = name;
			if (typeof cellId === "string" && name in this.terrains){
				cellId = this.terrains[name];
			}

			if (cellX >= 0 && cellY >= 0 && cellX < this.width && cellY < this.height){
				this.layers[layer].data[cellX][cellY] = cellId;
			}
		},

		getLayerIdByName: function(name){
			if (name in this.layerNameMap){
				return this.layerNameMap[name];
			}
			return -1;
		},

		renderLayer: function(layerId){
			var texture = new Pixi.RenderTexture(this.width * this.tilewidth, this.height * this.tileheight);

			var container = new Pixi.DisplayObjectContainer();

			this._loadTextures( (function(){
				var layer = this.layers[layerId];
				for (var x=0; x<layer.width; ++x){
					for (var y=0; y<layer.height; ++y){
						var tileId = layer.data[x][y];
						if (tileId > 0){
							var subtex = this._getTextureForCell(tileId);
							var sprite = new Pixi.Sprite(subtex);
							sprite.tint = this.tint;
							//sprite.filters = this.filters;
							sprite.position.x = x * this.tilewidth;
							sprite.position.y = y * this.tileheight;
							container.addChild(sprite);
						}
					}
				}
				texture.render(container);
			}).bind(this));


			return texture;
		},

		//Merges one TMX with another
		merge: function(tmx){
			var i;

			if (tmx.tilesets.length != this.tilesets.length)
				throw Error("Need equivilant tilesets");

			for (i=0; i<tmx.layers.length; ++i){
				this.layers.push(tmx.layers[i]);
			}

			for (i=0; i<tmx.boxes.length; ++i){
				this.boxes.push(tmx.boxes[i]);
			}
		},

		_getCellTileset: function(tileId)
		{
			//Get the largest base id that isn't greater than the tileId
			var largestId = 0;
			var tileset = null;
			for (var i=0; i<this.tilesets.length; ++i){
				var curr = this.tilesets[i];
				if (curr.startId > largestId && curr.startId <= tileId){
					largestId = curr.startId;
					tileset = curr;
				}
			}
			return tileset;
		},

		_getTextureForCell: function(tileId){
			var tileset = this._getCellTileset(tileId);

			//Compute texture rect
			var cellOffset = (tileId - tileset.startId) * tileset.width;
			var cellX = cellOffset % tileset._texture.width;
			var cellY = Math.floor(cellOffset / tileset._texture.width) * tileset.height;
			var subtex = new Pixi.Texture(tileset._texture.baseTexture, new Pixi.Rectangle(cellX,cellY,this.tilewidth,this.tileheight));

			return subtex;
		},

		_loadTextures: function(callback){
			var totalLoaded=0;

			var texLoaded = (function(){
					totalLoaded++;
					if (totalLoaded == this.tilesets.length)
						callback();
				}).bind(this);

			for (var i=0; i<this.tilesets.length; ++i){
				var tileset = this.tilesets[i];
				if (tileset._texture && tileset._texture.baseTexture.hasLoaded)
				{
					//Already loaded
					totalLoaded++;
				}
				else
				{
					var path = this._basePath + tileset.image;
					tileset._texture = Pixi.Texture.fromImage(path);
					tileset._texture.on("update", texLoaded);
				}
			}

			if (totalLoaded == this.tilesets.length)
				callback();
		}
	};

	return Tmx;
});