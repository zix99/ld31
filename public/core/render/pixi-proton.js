define(["lib/pixi", "lib/proton"], function(Pixi, proton){
	function transformSprite(sprite, particle){
		sprite.position.x = particle.p.x;
		sprite.position.y = particle.p.y;
		sprite.scale.x = particle.scale;
		sprite.scale.y = particle.scale;
		sprite.alpha = particle.alpha;
		sprite.rotation = particle.rotation * Math.PI / 180;
	}


	function PixiProton()
	{
		var stage = this._stage = new Pixi.DisplayObjectContainer();

		this._proton = new proton();
		var renderer = new proton.Renderer("other", this._proton);

		renderer.onProtonUpdate = function() {

		};
		renderer.onParticleCreated = function(particle) {
			var sprite = new Pixi.Sprite(particle.target);
			sprite.anchor.x = 0.5;
			sprite.anchor.y = 0.5;
			sprite.blendMode = particle.target.blendMode || Pixi.blendModes.NORMAL;
			particle._sprite = sprite;
			stage.addChild(sprite);
		};
		renderer.onParticleUpdate = function(particle) {
			transformSprite(particle._sprite, particle);
		};
		renderer.onParticleDead = function(particle) {
			stage.removeChild(particle._sprite);
		};
		renderer.start();
	}

	PixiProton.prototype = {
		getStage: function(){
			return this._stage;
		},
		getProton: function(){
			return this._proton;
		},
		add: function(emitter){
			this._proton.addEmitter(emitter);
		},
		update: function(){
			this._proton.update();
		}
	};

	return PixiProton;
});