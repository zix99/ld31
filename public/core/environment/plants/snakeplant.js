define(["core/environment/plants/spreadingbase"], function(SpreadingBase){
	function SnakePlant(pos){
		SpreadingBase.call(this, "content/sprites/environment/shrub2.png", pos, 0.01);
		this.edible = true;
	}

	SnakePlant.extends(SpreadingBase, {
		spawn: function(pos){
			return new SnakePlant(pos);
		}
	});

	return SnakePlant;
});