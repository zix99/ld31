define(["core/environment/plants/growthbase", "core/environment/plants/shrub", "core/environment/plants/snakeplant", "core/environment/plants/tree", "core/audio/sfx"],
function(GrowthBase, Shrub, SnakePlant, Tree, Sound){
	function Poop(pos){
		GrowthBase.call(this, "content/sprites/poop.png", pos, 12);
		Sound.play("plop-" + Math.randomInt(0, 4));
	}

	Poop.extends(GrowthBase, {
		grow: function(pos){
			var r = Math.random();
			if (r < 0.2)
				return new Shrub(pos);
			if (r < 0.4)
				return new SnakePlant(pos);
			if (r < 0.5)
				return new Tree(pos);
			return null;
		}
	});

	return Poop;
});