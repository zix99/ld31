define(["core/environment/envbase"], function(EnvBase){
	function GrowthBase(texture, pos, growthDelay){
		EnvBase.call(this, texture, pos);
		this._growthDelay = growthDelay;
		this._growtick = 0;
	}

	GrowthBase.extends(EnvBase, {
		update: function(){
			this._growtick++;
			if (this._growtick > this._growthDelay){
				var ent = this.grow(this.getPosition());
				if (ent)
					this.replace(ent);
			}
		},
		grow: function(){
			throw Error("Grow unimplemented");
		}
	});

	return GrowthBase;
});