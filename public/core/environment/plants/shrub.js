define(["core/environment/plants/spreadingbase"], function(SpreadingBase){
	function Shrub(pos){
		SpreadingBase.call(this, "content/sprites/environment/shrub1.png", pos, 0.01);
		this.edible = true;
	}

	Shrub.extends(SpreadingBase, {
		spawn: function(pos){
			return new Shrub(pos);
		}
	});

	return Shrub;
});