define(["context", "core/environment/envbase"], function(Context, EnvBase){
	function SpreadingBase(texture, pos, spreadChance){
		EnvBase.call(this, texture, pos);
		this._chance = spreadChance;

		this.edible = true;
	}

	function randomRange(min, max){
		return Math.random() * (max - min) + min;
	}

	SpreadingBase.extends(EnvBase, {
		update: function(){
			var cell = Context.scene._tmx.getCell(this._sprite.position.x, this._sprite.position.y);
			var spreadFactor = 1.0;
			if (cell)
				spreadFactor = typeof cell.props.growth === "undefined" ? 1.0 : +cell.props.growth;

			if (Math.random() < this._chance * spreadFactor){
				var x = Math.clamp(this._sprite.position.x + randomRange(-64, 64), 0, Context.view.width-1);
				var y = Math.clamp(this._sprite.position.y + randomRange(-64, 64), 0, Context.view.height-1);
				this.parent.add(this.spawn({x: x, y: y}));
			}
		},
		spawn: function(){
			throw Error("Virtual method spawn not implemented");
		}
	});

	return SpreadingBase;
});