define(["core/environment/plants/spreadingbase", "core/environment/plants/growthbase"], function(SpreadingBase, GrowthBase){
	function DeadTree(pos){
		GrowthBase.call(this, "content/sprites/environment/tree1_dead.png", pos, 14);
	}

	DeadTree.extends(GrowthBase, {
		grow: function(pos){
			return new Tree(pos);
		},
		//kill: function(){
		//	this.replace(new DeadTree(this.getPosition()));
		//}
	});



	function Tree(pos){
		SpreadingBase.call(this, "content/sprites/environment/tree1.png", pos, 0.008);
		this.edible = true;
	}

	Tree.extends(SpreadingBase, {
		spawn: function(pos){
			return new DeadTree(pos);
		},
		kill: function(){
			if (Math.random() < 0.9) {
				this.replace(new DeadTree(this.getPosition()));
			}
			else
			{
				this.EnvBase_kill();
			}
		}
	});

	return Tree;
});