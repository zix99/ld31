define(["context", "lib/victor.min", "lib/pixi", "core/environment/plants/tree", "core/environment/plants/shrub", "core/environment/plants/snakeplant", "core/environment/plants/poop", "lib/underscore"],
function(Context, Vector, Pixi, Tree, Shrub, SnakePlant, Poop, _){
	function Environment(){
		this._entities = [];
		this._stage = new Pixi.DisplayObjectContainer();

		this._tick=0;
		this._rate = 1;
	}

	Environment.prototype = {
		add: function(static){
			static.setParent(this);
			this._entities.push(static);
			this._stage.addChild(static.getSprite());
		},

		dropPoop: function(pos){
			this.add(new Poop(pos));
		},

		remove: function(static){
			static.setParent(null);
			this._entities = _.without(this._entities, static);
			this._stage.removeChild(static.getSprite());
		},

		getCount: function(){
			return this._entities.length;
		},

		getEntities: function(){
			return this._entities;
		},

		setRate: function(rate){
			this._rate = rate;
		},

		getEdibleNear: function(vec, near){
			var pos = vec.clone();
			return this.getOfType(function(e){
				return e.edible && e.getPosition().distance(pos) < near;
			});
		},

		getTotemNear: function(vec){
			var pos = vec.clone();
			return this.getOfType(function(e){
				return e.totemRange && e.getPosition().distance(pos) < e.totemRange;
			});
		},

		getOfType: function(predicate){
			for (var i=0; i<this._entities.length; ++i){
				var entity = this._entities[i];
				if (predicate(entity))
					return entity;
			}
			return null;
		},

		update: function(){
			this._tick += this._rate;
			if (this._tick >= 120){
				this._tick = 0;

				for (var i=0; i<this._entities.length; ++i){
					if (this._entities[i].update)
						this._entities[i].update();
				}
			}
		},

		getStage: function(){
			return this._stage;
		},

		generate: function(amt){
			for (var i=0; i<amt; ++i){
				var r = Math.random();
				var pos = {
					x: Math.random() * Context.view.width,
					y: Math.random() * Context.view.height
				};

				if (r < 0.3){
					this.add(new Tree(pos));
				}
				else if (r < 0.7)
				{
					this.add(new Shrub(pos));
				}
				else
				{
					this.add(new SnakePlant(pos));
				}
			}
		}
	};

	return Environment;
});