define(["lib/pixi", "lib/victor.min"], function(Pixi, Vector){
	function EnvBase(texture, position){
		var tex = Pixi.Texture.fromImage(texture);
		this._sprite = new Pixi.Sprite(tex);
		this._sprite.position.x = position.x;
		this._sprite.position.y = position.y;
		this._sprite.anchor.x = 0.5;
		this._sprite.anchor.y = 0.5;
	}

	EnvBase.prototype = {
		getSprite: function(){
			return this._sprite;
		},
		setParent: function(parent){
			this.parent = parent;
		},
		getPosition: function(){
			return new Vector(this._sprite.position.x, this._sprite.position.y);
		},
		kill: function(){
			this.parent.remove(this);
		},
		replace: function(ent){
			this.parent.add(ent);
			this.parent.remove(this);
		}
	};

	return EnvBase;
});