define(["context", "core/pfx/fireball", "core/audio/sfx"], function(Context, Pfx, Sound){
	var radius = 48;

	function Fireball(pos){
		var emit = new Pfx(pos);
		Context.particles.add(emit);
		emit.emit("once");

		Sound.play("exp-" + Math.randomInt(0,4));

		//damage things
		var entities = Context.scene._entities.getEntities();
		for (var i=0; i<entities.length; ++i){
			var entity = entities[i];
			if (entity.position().distance(pos) < radius)
			{
				entity.damage(100);
			}
		}

		var foliage = Context.environment.getEntities();
		for (var j=0; j<foliage.length; ++j){
			var f = foliage[j];
			if (f.getPosition().distance(pos) < radius)
				f.kill();
		}
	}

	return Fireball;
});