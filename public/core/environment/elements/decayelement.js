define(["core/environment/envbase"], function(EnvBase){
	function DecayElement(texture, pos, decay){
		EnvBase.call(this, texture, pos);
		this._decay = decay;
		this._decayTick = 0;
	}

	DecayElement.extends(EnvBase, {
		update: function(){
			this._decayTick++;
			if (this._decayTick >= this._decay){
				var repl = this.decayed(this.getPosition());
				if (repl)
					this.replace(repl);
				else
					this.kill();
			}
		},
		decayed: function(){
			return null;
		}
	});

	return DecayElement;
});