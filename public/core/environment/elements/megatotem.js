define(["core/environment/elements/decayelement"], function(DecayElement){
	function MegaTotem(pos){
		DecayElement.call(this, "content/sprites/totem2.png", pos, 12);
		this.totemRange = 128;
	}

	MegaTotem.extends(DecayElement, {

	});

	return MegaTotem;
});