define(["core/environment/elements/decayelement"], function(DecayElement){
	function Totem(pos){
		DecayElement.call(this, "content/sprites/totem.png", pos, 7);
		this.totemRange = 64;
	}

	Totem.extends(DecayElement, {

	});

	return Totem;
});