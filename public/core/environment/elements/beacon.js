define(["core/environment/elements/decayelement"], function(DecayElement){
	function Beacon(pos){
		DecayElement.call(this, "content/sprites/beacon.png", pos, 14);
	}

	Beacon.extends(DecayElement, {

	});

	return Beacon;
});