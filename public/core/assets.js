define(["lib/pixi"], function(Pixi){
	var pixiAssets = [
		"/content/sprites/characters/officeman2.png",
		"/content/sprites/dots.png"
	];

	function loadPixiAssets(callback)
	{
		var assetLoader = new Pixi.AssetLoader(pixiAssets);
		assetLoader.on("onComplete", callback);
		assetLoader.load();
	}

	function loadAudioAssets(callback)
	{
		callback();
	}

	return {
		load: function(callback){
			loadPixiAssets(function(){
				loadAudioAssets(function(){
					callback();
				});
			});
		}
	};
});