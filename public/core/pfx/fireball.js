define(["lib/pixi", "lib/proton"], function(Pixi, Proton){
	return function(pos) {
		var image = Pixi.Texture.fromImage("content/particles/fire.png");
		image.blendMode = Pixi.blendModes.ADD;

		var emitter = new Proton.Emitter();
		emitter.rate = new Proton.Rate(new Proton.Span(10, 20), 0.1);
		//emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.ImageTarget(image));
		emitter.addInitialize(new Proton.Position(new Proton.CircleZone(pos.x, pos.y, 10)));
		emitter.addInitialize(new Proton.Life(5, 7));
		emitter.addInitialize(new Proton.V(1, new Proton.Span(0, 360), "polar"));
		emitter.addBehaviour(new Proton.Scale(1, 0.2));
		emitter.addBehaviour(new Proton.Alpha(1, 0.2));

		return emitter;
	};
});