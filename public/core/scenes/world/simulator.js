define(["context"], function(Context){
	function WorldSimulator(world){
		this._world = world;
	}

	WorldSimulator.prototype = {
		start: function(){
			this._dirty = false;

			window.setInterval(this._iterate.bind(this), 100);
			window.setInterval(this._redraw.bind(this), 500);
		},

		_iterate: function(){
			var x = Math.random() * this._world.screenWidth;
			var y = Math.random() * this._world.screenHeight;

			var cell = this._world.getLayerCell(0, x,y);
			if (cell.props.spread){
				var dir = this._getRandDirection();
				this._world.setCellByName(x+dir[0]*this._world.tilewidth, y+dir[1]*this._world.tileheight, 0, cell.id);
				this._dirty = true;
			}
		},

		_redraw: function(){
			if (this._dirty){
				Context.scene.render();
				this._dirty = false;
			}
		},

		_getRandDirection: function(){
			var x= Math.random() > 0.5 ? 1 : -1;
			var y = Math.random() > 0.5 ? 1 : -1;
			return [x,y];
		}
	};

	return WorldSimulator;
});