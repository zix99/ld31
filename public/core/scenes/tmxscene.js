define(["lib/pixi", "core/render/tmx", "core/scenes/scenebase"], function(Pixi, Tmx, SceneBase){
	function TmxScene(xml, basePath){
		SceneBase.call(this);

		this._internalStage = this._stage;
		this._stage = new Pixi.DisplayObjectContainer();
		this._internalStage.addChild(this._stage);

		this._tmx = new Tmx(xml, basePath);
		this.render();
	}

	TmxScene.extends(SceneBase, {
		getStage: function(){
			return this._internalStage;
		},
		getWorld: function(){
			return this._tmx;
		},
		setCameraPosition: function(x,y){
			this._stage.position.x = -x;
			this._stage.position.y = -y;
		},
		setTint: function(tint){
			this._tmx.tint = tint;
			this.render();
		},
		render: function(){
			var i;

			if (this._spriteLayers){
				for (i=0; i<this._spriteLayers.length; ++i){
					this._stage.removeChild(this._spriteLayers[i]);
					this._spriteLayers[i].texture.destroy();
				}
			}

			this._spriteLayers = [];

			for (i=0; i<this._tmx.layers.length; ++i){
				var tex = this._tmx.renderLayer(i);
				var sprite = new Pixi.Sprite(tex);
				this._stage.addChildAt(sprite, i);
				this._spriteLayers.push(sprite);
			}
		}
	});

	return TmxScene;
});