define(
["core/scenes/tmxscene",
"text!content/scenes/testterrain.tmx",
"lib/pixi",
"core/entityManager",
"core/entity/bunny",
"core/entity/person",
"core/pfx/fire",
"core/audio/sfx",
"core/lib/keyboard",
"lib/proton",
"core/render/pixi-proton",
], function(TmxBase, TmxData, Pixi, EntityManager, Bunny, Person, Fire,  Audio, Keyboard, Proton, PixiProton){
	function TestScene(){
		TmxBase.call(this, TmxData, "/content/scenes/");
	}

	TestScene.extends(TmxBase, {
		load: function(callback){
			var entities = this._entities = new EntityManager(this._stage);

			var bunny = new Bunny();
			entities.add(bunny);

			var player = this._player = new Person();
			player.world = this.getWorld();
			entities.add(player);

			this._particles = PixiProton(this._stage);	

			/*
			var texture = new Pixi.Texture.fromImage("content/sprites/bunny.png");
			var emitter = new Proton.BehaviourEmitter();
			emitter.rate = new Proton.Rate(new Proton.Span(15, 30), new Proton.Span(0.2, 0.5));
			emitter.addInitialize(new Proton.Mass(1));
			emitter.addInitialize(new Proton.ImageTarget(texture));
			emitter.addInitialize(new Proton.Life(2, 3));
			emitter.addInitialize(new Proton.Velocity(new Proton.Span(3, 9), new Proton.Span(0, 30, true), "polar"));

			emitter.addBehaviour(new Proton.Gravity(8));
			emitter.addBehaviour(new Proton.Scale(new Proton.Span(1, 3), 0.3));
			emitter.addBehaviour(new Proton.Alpha(1, 0.5));
			emitter.addBehaviour(new Proton.Rotate(0, Proton.getSpan(-8, 9), "add"));
			emitter.p.x = 1003 / 2;
			emitter.p.y = 100;
			emitter.emit();

			var fire = new Fire();
			psys.addEmitter(fire);

			(new Task(function(){
				fire.emit("once");
			})).start(2000);
*/

			var text = this._text = new Pixi.Text("Hi thar!", {font: "35px Arbo", fill: "#ff0000", stroke: "#0000ff", dropShadow: true});
			text.position.x = 100;
			text.position.y = 100;
			text.anchor.x = 0.5;
			text.anchor.y = 1;
			text.rotation = 20;
			this._stage.addChild(text);

			//psys.addEmitter(emitter);

			//var sfxBong = new Task(function(){
			//	Audio.play("bong");
			//});
			//sfxBong.start(10000);

			this.SceneBase_load(callback);
		},

		unload: function(){

		},

		update: function(){
			//this.setCameraPosition(this._player._sprite.position.x - 150, this._player._sprite.position.y - 150);
			this._particles.update();
			this._entities.update();
			this._text.rotation += 0.1;
		}
	});

	return TestScene;
});