define(
[
	"context",
	"core/scenes/tmxscene",
	"text!content/scenes/onescreen.tmx",
	"lib/pixi",
	"core/entityManager",
	"core/entity/herbivore",
	"core/entity/carnivore",
	"core/ui/root",
	"lib/victor.min",
	"core/scenes/world/simulator",
	"core/environment/environment",
	"core/render/pixi-proton"
],
function(Context, TmxBase, TmxData, Pixi, EntityManager, Herbivore, Carnivore, RootScreen, Vector, WorldSimulator, Environment, Particles){
	function OneScreenScene(){
		TmxBase.call(this, TmxData, "/content/scenes/");
		Context.world = this._tmx;
	}

	OneScreenScene.extends(TmxBase, {
		load: function(){
			var simulator = new WorldSimulator(this._tmx);
			simulator.start();

			var entities = this._entities = new EntityManager();
			this._stage.addChild(entities.getStage());

			var env = this._environment = new Environment();
			Context.environment = env;
			this._stage.addChild(env.getStage());
			env.generate(50);

			Context.particles = this._particles = new Particles();
			this._stage.addChild(this._particles.getStage());

			this.SceneBase_load();

			this._ui = new RootScreen();
			Context.messages = this._ui.messages;

			Context.game.start();
		},
		update: function(){
			this._entities.update();
			this._environment.update();
			this._particles.update();
		}
	});
	return OneScreenScene;
});