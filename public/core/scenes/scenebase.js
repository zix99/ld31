define(["lib/pixi"], function(Pixi){
	function SceneBase(){
		this._stage = new Pixi.Stage(0x66FF99);
	}

	SceneBase.prototype = {
		load: function(callback){
			if (typeof callback === "function")
				callback();
		},
		getStage: function(){
			return this._stage;
		}
	};

	return SceneBase;
});