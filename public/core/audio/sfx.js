define(["lib/soundjs", "core/lib/logger"], function(soundjs, log){
	if (!soundjs.initializeDefaultPlugins())
	{
		return null;
	}

	var audioPath = "/content/audio/";

	var manifest = [
		{id: "bong", src: "bong.ogg"},
		{id: "blop-1", src: "blop1.ogg"},
		{id: "blop-2", src: "blop2.ogg"},
		{id: "blop-3", src: "blop3.ogg"},
		{id: "blop-4", src: "blop4.ogg"},
		{id: "blop-5", src: "blop5.ogg"},

		{id: "darwinism-1", src: "darwinism1.ogg"},
		{id: "darwinism-2", src: "darwinism2.ogg"},
		{id: "darwinism-3", src: "darwinism3.ogg"},
		{id: "darwinism-4", src: "darwinism4.ogg"},
		{id: "darwinism-5", src: "darwinism5.ogg"},
		{id: "darwinism-6", src: "darwinism6.ogg"},

		{id: "doink-1", src: "doink1.ogg"},
		{id: "doink-2", src: "doink2.ogg"},
		{id: "doink-3", src: "doink3.ogg"},
		{id: "doink-4", src: "doink4.ogg"},
		{id: "doink-5", src: "doink5.ogg"},

		{id: "meh-1", src: "meh1.ogg"},
		{id: "meh-2", src: "meh2.ogg"},
		{id: "meh-3", src: "meh3.ogg"},
		{id: "meh-4", src: "meh4.ogg"},
		{id: "meh-5", src: "meh5.ogg"},

		{id: "nom-1", src: "nom1.ogg"},
		{id: "nom-2", src: "nom2.ogg"},
		{id: "nom-3", src: "nom3.ogg"},
		{id: "nom-4", src: "nom4.ogg"},

		{id: "plop-1", src: "plop1.ogg"},
		{id: "plop-2", src: "plop2.ogg"},
		{id: "plop-3", src: "plop3.ogg"},

		{id: "exp-1", src: "exp1.ogg"},
		{id: "exp-2", src: "exp2.ogg"},
		{id: "exp-3", src: "exp3.ogg"},

		{id: "kaching", src: "kaching.ogg"},
		{id: "debt", src:"notenoughpoints.ogg"},
	];

	function handleLoad(event){
		log.logInfo("Loaded %s", event.src);
	}

	soundjs.alternateExtensions = ["mp3"];
	soundjs.addEventListener("fileload", handleLoad);
	soundjs.registerManifest(manifest, audioPath);

	return soundjs;
});