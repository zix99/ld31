define(
["context",
"lib/pixi",
"core/scenes/onescreen",
"core/assets",
"lib/stats",
"core/lib/logger",
"core/lib/task",
"core/game/gamecontext"],
function(
Context,
pixi,
Scene,
Assets,
Stats,
logger,
Task,
GameContext){
	function game(dom)
	{
		this.ele = dom;
	}

	game.prototype = {
		start: function(){
			logger.logInfo("Starting screen...");
			var renderer = new pixi.autoDetectRenderer(1024, 512);
			Context.view = renderer.view;
			this.ele.appendChild(renderer.view);

			logger.logInfo("Setting up stats...");
			var statsFps = new Stats();
			statsFps.setMode(0); //FPS
			statsFps.domElement.style.position = "absolute";
			statsFps.domElement.style.left = "0px";
			statsFps.domElement.style.top = "0px";

			var statsUpdate = new Stats();
			statsUpdate.setMode(2);
			statsUpdate.domElement.style.position = "absolute";
			statsUpdate.domElement.style.left = "0px";
			statsUpdate.domElement.style.top = "80px";

			if (!window.env.prod)
			{
				this.ele.appendChild(statsFps.domElement);
				this.ele.appendChild(statsUpdate.domElement);
			}
			Assets.load(function(){
				Context.game = new GameContext();

				logger.logInfo("Setting up stage...");
				var scene = Context.scene = new Scene();
				scene.load();

				var simulation = new Task(function(){
					statsUpdate.begin();
					scene.update();
					statsUpdate.end();
				});
				simulation.start(16);

				var animate = function()
				{
					window.requestAnimFrame(animate);

					statsFps.begin();
					renderer.render(scene.getStage());
					statsFps.end();
				};
				window.requestAnimFrame(animate);
			});
		},

	};

	return game;
});