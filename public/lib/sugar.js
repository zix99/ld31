(function(){

	function _inheritPrototype(that, baseclass){
		for(var key in baseclass.prototype){
			if (typeof baseclass.prototype[key] === 'function'){
				that.prototype[baseclass.name + '_' + key] = baseclass.prototype[key];
			}
		}
	}

	Function.prototype.extends = function(baseclass, methods){
		var i=0;

		if (baseclass instanceof Array){
			this.prototype = {};
			for(i=0; i<baseclass.length; ++i){
				var instObj = Object.create(baseclass[i].prototype);
				for (var pItem in instObj){
					this.prototype[pItem] = instObj[pItem];
				}
			}
		} else {
			this.prototype = Object.create(baseclass.prototype);
		}
		
		for(var key in methods){
			this.prototype[key] = methods[key];
		}

		this.prototype.constructor = this;
		Object.defineProperty(this.prototype, 'constructor', { 
			enumerable: false, 
			value: this 
		});

		if (baseclass instanceof Array){
			for (i=0; i<baseclass.length; ++i){
				_inheritPrototype(this, baseclass[i]);
			}
		}
		else
		{
			_inheritPrototype(this, baseclass);
		}

		return this;
	};
})();