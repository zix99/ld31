(function(){
	Math.clamp = function(val, min, max){
		return val < min ? min : (val > max ? max : val);
	};

	Math.randomInt = function(min, max){
		return Math.floor(Math.random() * (max-min) + min);
	};

	Math.randomRange = function(min, max){
		return Math.random() * (max - min) + min;
	};

	function _getInternalName(a){
		if (typeof a === "string")
			return a;
		if (typeof a === "object")
			return a.constructor.name;
		if (typeof a === "undefined")
			return null;
		return a;
	}

	window.isSameAs = function(a, b){
		return _getInternalName(a) === _getInternalName(b);
	};

})();