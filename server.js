var express = require("express");
var logger = require("morgan");
var hbs = require("express3-handlebars");
var app = express();

var args = require("minimist")(process.argv.slice(2));

var isProd = args.prod || false;
if (isProd)
{
	console.log("Executing in PRODUCTION mode");
}

app.set("views", __dirname + "/views");

app.engine("hbs", hbs({}) );
app.set("view engine", "hbs");

app.get("/", function(req, res){
	var model = {prod: isProd};
	res.render("index", model);
});

app.use(express.static(__dirname + "/public"));
app.use(logger("combined"));

var port = args.p || args.port || 15553;

console.log("Server starting on port " + port);
app.listen(port);
